package controllers

import models.{Place, Location}
import play.api.libs.json._
import play.api.libs.functional.syntax._
/**
  * Created by konrad on 2/6/16.
  */
object JsonProcessing {

  implicit val locationWrites: Writes[Location] = (
      (JsPath \ "lat").write[Double] and
      (JsPath \ "long").write[Double]
    )(unlift(Location.unapply))



  implicit val locationReads: Reads[Location] = (
      (JsPath \ "lat").read[Double] and
      (JsPath \ "long").read[Double]
    )(Location.apply _)




}
