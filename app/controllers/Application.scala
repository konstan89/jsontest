package controllers

import models._
import play.api._
import play.api.libs.iteratee.Enumerator
import play.api.mvc._
import play.api.mvc._
import play.api.libs.json._
import JsonProcessing._


class Application extends Controller {

  val index = Action {
    Result(
      header = ResponseHeader(200, Map(CONTENT_TYPE -> "text/plain")),
      body = Enumerator("Hello world!".getBytes())
    )

  }

  val listPlaces = Action {
    val json = Json.toJson(Place.list)
    Ok(json)
  }


  val savePlaces = Action(BodyParsers.parse.json) { request =>

    val placeResult = request.body.validate[Place]
    placeResult.fold(
      errors => {
        BadRequest(Json.obj("status" ->"KO", "message" -> JsError.toFlatJson(errors)))
      },
      place => {
        Place.save(place)
        Created(Json.obj("status" ->"Created", "message" -> ("Place '"+place.name+"' saved.") ))
      }
    )


  }

}
