package models

import play.api.libs.json._
import play.api.libs.functional.syntax._

/**
  * Created by konrad on 2/6/16.
  */

case class Location(lat:Double,long:Double)

case class Place(name:String,location:Location)

object Place {

  var list: List[Place] = {
    List (
      Place(
        "Warszawa",
        Location(1231.123, 123.124)
      ),

      Place (
        "Poznan",
        Location(657.56,1853.1)

      )
    )


  }

  def save(place:Place) = {

    list = list ::: List(place)
  }




     implicit val locationWrites: Writes[Location] = (
        (JsPath \ "lat").write[Double] and
        (JsPath \ "long").write[Double]
      )(unlift(Location.unapply))



    implicit val locationReads: Reads[Location] = (
        (JsPath \ "lat").read[Double] and
        (JsPath \ "long").read[Double]
      )(Location.apply _)

    implicit val placeReads: Reads[Place] = (
        (JsPath \ "name").read[String] and
        (JsPath \ "location").read[Location]
      )(Place.apply _)


    implicit val placeWrites: Writes[Place] = (
        (JsPath \ "name").write[String] and
        (JsPath \ "location").write[Location]
      )(unlift(Place.unapply))









}

